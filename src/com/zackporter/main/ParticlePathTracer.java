package com.zackporter.main;

import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.zackporter.particles.ParticleDisplay;
import com.zackporter.particles.ParticleManager;
import com.zackporter.particles.SpawnMode;

import jutils.jy89hui.Slick2DTools.Display;
import jutils.jy89hui.Slick2DTools.Specialized.MandelbrotSet;
import jutils.jy89hui.tests.DisplayTester;

public class ParticlePathTracer extends Display{
	public ParticlePathTracer(String title) {
		super(title);
	}
	static ParticleDisplay pdisplay;
	static ParticleManager pmanager;
	final static int WIDTH=1500, HEIGHT=1000;
	public static void main(String[] args){
		ImageReader ir = new ImageReader();
		List<int[]> positions = ir.readPositions("/Users/Zack/Desktop/HelloMalia2.png",5);
		
		pmanager = new ParticleManager(22000, WIDTH,HEIGHT,
				SpawnMode.CIRCLE, SpawnMode.CIRCLE, 100, 500);
		//pmanager = new ParticleManager(WIDTH,HEIGHT,positions);
		
		Display.inialize(new ParticlePathTracer("Particle Movement"), WIDTH,HEIGHT);
	}

	@Override
	public void initialize(GameContainer gc) {
		this.getComponentManager().setBackgroundColor(new Color(113,185,209));
		pdisplay = new ParticleDisplay(0,0,WIDTH,HEIGHT,pmanager);
		getComponentManager().add(pdisplay);
	}

	@Override
	public void postComponentDraw(GameContainer gc, Graphics g) {
		
	}

	@Override
	public void postComponentUpdate(GameContainer gc) {
		
	}

	@Override
	public void eventKeyPressed(int key, char c) {
	}

	@Override
	public void eventMouseClicked(int button, int locx, int locy, int clickcount) {
	}

	@Override
	public void eventMouseWheelMoved(int amount) {
	}
}

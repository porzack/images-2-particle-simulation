package com.zackporter.main;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;


import jutils.jy89hui.general.logger;

public class ImageReader {
	public List<int[]> readPositions(String file, int resolution) {
		List<int[]> positions = new ArrayList<>();
		File f = new File(file);
		BufferedImage image = null;
		try {
			image = ImageIO.read(f);
		} catch (IOException e) {
			logger.log("FAILED TO READ FILE: "+file);
			e.printStackTrace();
		}
		for (int x=0;; x+=resolution) {
				for (int y=0;; y+=resolution) {
					try{
						int c =image.getRGB(x, y);
						Color color = new Color(c);
						boolean white=color.getRed()+color.getGreen()+color.getBlue()>3*200?true:false;
						if (!white) {
							positions.add(new int[] {x,y});
						}
						if (y%100==0) {
						//	System.out.println(y);
						}
					}catch(ArrayIndexOutOfBoundsException e) {
						break;
					}
				}
				try{
					image.getRGB(x+1,0);
				}catch(ArrayIndexOutOfBoundsException e) {
					break;
				}
		}
		positions.add(new int[] {image.getWidth(), image.getHeight()});
		return positions;
	}
}

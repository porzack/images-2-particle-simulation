package com.zackporter.particles;

public enum P {
	POS_X(0),
	POS_Y(1),
	VEL_X(2),
	VEL_Y(3),
	DES_X(4),
	DES_Y(5),
	LENGTH(6);
	public final int v;
	P(int val){
		this.v=val;
	}
}

package com.zackporter.particles;

import jutils.jy89hui.general.logger;

public enum SpawnMode {
	RANDOM {
		@Override
		public double spawnX(double width, double radius, double rand, double rand2) {
			return rand*width;
		}

		@Override
		public double spawnY(double height, double radius, double rand, double rand2) {
			return rand2*height;
		}
	},
	CIRCLE {
		@Override
		public double spawnX(double width, double radius, double rand, double rand2) {
			return Math.cos(rand*2*3.1415)*radius+width/2;
		}

		@Override
		public double spawnY(double height, double radius, double rand, double rand2) {
			return Math.sin(rand*2*3.1415)*radius+height/2;
		}
	},
	CENTER {
		@Override
		public double spawnX(double width, double radius, double rand, double rand2) {
			return width/2;
		}

		@Override
		public double spawnY(double height, double radius, double rand, double rand2) {
			return height/2;
		}
	},
	IMAGE {
		@Override
		public double spawnX(double width, double radius, double rand, double rand2) {
			logger.log("Spawnmode IMAGE spawnX() called. Spawnmode does not work with this. ");
			return 0;
		}

		@Override
		public double spawnY(double height, double radius, double rand, double rand2) {
			logger.log("Spawnmode IMAGE spawnY() called. Spawnmode does not work with this. ");
			return 0;
		}
	},
	GRID {
		@Override
		public double spawnX(double width, double radius, double rand, double rand2) {
			logger.log("Spawnmode GRID spawnX() called. Spawnmode does not work with this. ");
			return 0;
		}

		@Override
		public double spawnY(double height, double radius, double rand, double rand2) {
			logger.log("Spawnmode GRID spawnY() called. Spawnmode does not work with this. ");
			return 0;
		}
	};
	SpawnMode(){
		
	}
	public abstract double spawnX(double width, double radius, double rand, double rand2);
	public abstract double spawnY(double height, double radius, double rand, double rand2);
}

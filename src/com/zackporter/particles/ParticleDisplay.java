package com.zackporter.particles;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.zackporter.main.ImageReader;

import jutils.jy89hui.Slick2DTools.VisualComponent;
import jutils.jy89hui.general.logger;

public class ParticleDisplay extends VisualComponent{
	private int framecount=0;
	private ParticleManager particleManager;
	public ParticleDisplay(int x, int y, int w, int h, ParticleManager pm) {
		this.setBounds(x, y, w, h);
		this.particleManager = pm;
	}
	@Override
	public void render(GameContainer gc, Graphics g) {
		List<double[]> particles = particleManager.getParticles();
		g.setColor(Color.white);
		g.drawString("Particles: "+particles.size(), 10, 30);
		g.setColor(Color.black);
		for(double[] p : particles) {
			g.fillRect((int)p[P.POS_X.v]+this.getXLoc(), (int)p[P.POS_Y.v]+this.getYLoc(), 5, 5);
		}
		g.setColor(Color.cyan);
		for(double[] p : particles) {
		//	g.fillRect((int)p[P.DES_X.v]+this.getXLoc(), (int)p[P.DES_Y.v]+this.getYLoc(), 4, 4);
		}
		
		if (framecount++==0) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	private long last=0;
	@Override
	public void update(GameContainer gc) {
		last=last==0?System.nanoTime()-10:last;
		long current = System.nanoTime();
		double secondDiff=(current-last)/1000000000.0;
		particleManager.update(secondDiff);
		last=current;
	}
	private List<List<int[]>> maps = new ArrayList<>();
	private int currentMap=-1;
	@Override
	public void init(GameContainer gc) {
		ImageReader ir = new ImageReader();
		maps.add(ir.readPositions("/Users/Zack/Desktop/Dad.png",2));
		maps.add(ir.readPositions("/Users/Zack/Desktop/NY.png",3));
		maps.add(ir.readPositions("/Users/Zack/Desktop/SoCool.png",3));
	/*	maps.add(ir.readPositions("/Users/Zack/Desktop/dl2.png",1));
		maps.add(ir.readPositions("/Users/Zack/Desktop/1.png",2));
		maps.add(ir.readPositions("/Users/Zack/Desktop/2.png",4));
		maps.add(ir.readPositions("/Users/Zack/Desktop/3.png",4));
		maps.add(ir.readPositions("/Users/Zack/Desktop/dl2.png",1));*/
		
		
	}

	@Override
	public void onKeyPress(int key, char c) {
		if (key==Input.KEY_S) {
			particleManager.shuffleVelocities(100000);
		}
		if (key == Input.KEY_P) {
			currentMap++;
			particleManager.changeDestinations(maps.get(currentMap));
		}
		if (key == Input.KEY_O) {
			currentMap--;
			particleManager.changeDestinations(maps.get(currentMap));
		}
		if (key == Input.KEY_C) {
			particleManager.changeToCircle(500);
		}
	}

	@Override
	public void mouseWheelMoved(int val) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clicked(int button) {
		// TODO Auto-generated method stub
		
	}

}

package com.zackporter.particles;

import java.util.ArrayList;
import java.util.List;

import jutils.jy89hui.general.logger;

public class ParticleManager {
	private List<double[]> particles = new ArrayList<double[]>();
	private int width,height;
	public ParticleManager(int numparticles, int width, int height) {
		this.width=width;
		this.height=height;
		double ratio = (double)width/height;
		int numRow = (int) Math.ceil(Math.sqrt(numparticles/ratio));
		int numCol = (int) Math.ceil(numRow*ratio);
		double multX = (double)width/numCol;
		double multY = (double)height/numRow;
		for(int x=0; x<numCol;x++) {
			for(int y=0; y<numRow; y++) {
				double[] particle = new double[P.LENGTH.v];
				double a = Math.random()*2*3.1415;
				particle[P.POS_X.v]=(x*multX);
				particle[P.POS_Y.v]=(y*multY);
				particle[P.VEL_X.v]=0;
				particle[P.VEL_Y.v]=0;
				particle[P.DES_X.v]=Math.cos(a)*200+width/2;
				particle[P.DES_Y.v]=Math.sin(a)*200+height/2;
				particles.add(particle);
			}
		}
	}
	public ParticleManager(int numparticles, int width, int height, SpawnMode pos, SpawnMode des, int radius, int radius2) {
		
		this.width=width;
		this.height=height;
		double ratio = (double)width/height;
		int numRow = (int) Math.ceil(Math.sqrt(numparticles/ratio));
		int numCol = (int) Math.ceil(numRow*ratio);
		double multX = (double)width/numCol;
		double multY = (double)height/numRow;
		for(int x=0; x<numCol;x++) {
			for(int y=0; y<numRow; y++) {
				double[] particle = new double[P.LENGTH.v];
				double r1 = Math.random();
				double r2 = Math.random();
				double r3 = Math.random();
				double r4 = Math.random();
				if (pos==SpawnMode.GRID) {
					particle[P.POS_X.v]=(x*multX);
					particle[P.POS_Y.v]=(y*multY);
				}else {
					particle[P.POS_X.v]=pos.spawnX(width, radius, r1, r2);
					particle[P.POS_Y.v]=pos.spawnY(height, radius, r1, r2);
				}
				particle[P.VEL_X.v]=0;
				particle[P.VEL_Y.v]=0;
				particle[P.DES_X.v]=des.spawnX(width, radius2, r3, r4);
				particle[P.DES_Y.v]=des.spawnY(height, radius2, r3, r4);
				particles.add(particle);
			}
		}
	}
	public ParticleManager(int width, int height, List<int[]> destinations) {
		this.width=width;
		this.height=height;
		int numparticles = destinations.size()-1;
		double ratio = (double)width/height;
		int numRow = (int) Math.floor(Math.sqrt(numparticles/ratio));
		int numCol = (int) Math.floor(numRow*ratio);
		double multX = (double)width/numCol;
		double multY = (double)height/numRow;
		int orgMaxX = destinations.get(destinations.size()-1)[0];
		int orgMaxY = destinations.get(destinations.size()-1)[1];
		double scaleX = (double)width/orgMaxX;
		double scaleY = (double)height/orgMaxY;
		int particlenum = 0;
		for(int x=0; x<numCol;x++) {
			for(int y=0; y<numRow; y++) {
				double[] particle = new double[P.LENGTH.v];
				double a = Math.random()*2*3.1415;
				particle[P.POS_X.v]=(x*multX);
				particle[P.POS_Y.v]=(y*multY);
				particle[P.VEL_X.v]=0;
				particle[P.VEL_Y.v]=0;
				particle[P.DES_X.v]=destinations.get(particlenum)[0]*scaleX;
				particle[P.DES_Y.v]=destinations.get(particlenum)[1]*scaleY;
				particles.add(particle);
				particlenum++;
			}
		}
	}
	public void changeDestinations(List<int[]> destinations) {
		int orgMaxX = destinations.get(destinations.size()-1)[0];
		int orgMaxY = destinations.get(destinations.size()-1)[1];
		double scaleX = (double)width/orgMaxX;
		double scaleY = (double)height/orgMaxY;
		int desSize = destinations.size()-1;
		
		if (particles.size()>desSize) {
			int excess = particles.size()-desSize;
			for(int i=0;i<excess;i++) {
				particles.remove(particles.size()-1);
			}
		}
		else if (desSize>particles.size()) {
			int required = desSize-particles.size();
			for (int i=0; i<required;i++) {
				double[] particle = new double[P.LENGTH.v];
				particle[P.POS_X.v]=width/2;
				particle[P.POS_Y.v]=height/2;
				particle[P.VEL_X.v]=0;
				particle[P.VEL_Y.v]=0;
				particle[P.DES_X.v]=0;
				particle[P.DES_Y.v]=0;
				particles.add(particle);
			}
		}
		this.shuffleVelocities(10000);
		
		int i=0;
		for (double[] p:particles) {
			p[P.DES_X.v]=destinations.get(i)[0]*scaleX;
			p[P.DES_Y.v]=destinations.get(i)[1]*scaleY;
			i++;
		}
		
	}
	public void changeToCircle(int radius) {
		for (double[] p:particles) {
			double a = Math.random()*2*3.1415;
			p[P.DES_X.v]=Math.cos(a)*radius+width/2;
			p[P.DES_Y.v]=Math.sin(a)*radius+height/2;
		}
	}
	public void shuffleVelocities(double scalar) {
		for (double[] p:particles) {
			p[P.VEL_X.v]+=(Math.random()-Math.random())*scalar;
			p[P.VEL_Y.v]+=(Math.random()-Math.random())*scalar;
		}
	}
	
	public List<double[]> getParticles(){
		return this.particles;
	}
	public void setParticles(List<double[]> particles) {
		this.particles=particles;
	}
	public void addDesiredForceVel(double[] p) {
		double dx = p[P.POS_X.v] - p[P.DES_X.v];
		double dy = p[P.POS_Y.v] - p[P.DES_Y.v];
		double hyp = Math.sqrt((dx*dx+dy*dy));
		hyp=Math.pow(hyp, 0.3);
		dx*=hyp;
		dy*=hyp;
		p[P.VEL_X.v]-=dx;
		p[P.VEL_Y.v]-=dy;
	}
	public void addFriction(double[] p) {
		p[P.VEL_X.v]*=0.8;
		p[P.VEL_Y.v]*=0.8;
	}
	public void addRepulsion(double[] p) {
		for(double[] o : particles) {
			if(Math.abs(o[P.POS_X.v]-p[P.POS_X.v])>0.1 && 
					Math.abs(o[P.POS_Y.v]-p[P.POS_Y.v])>0.1) {
				double dx = o[P.POS_X.v] - p[P.POS_X.v];
				double dy = o[P.POS_Y.v] - p[P.POS_Y.v];
				double hyp = Math.sqrt(dx*dx+dy*dy);
				hyp=hyp>3?hyp:3;
				double force = 1000/(hyp*hyp*hyp);
				dx=(dx/hyp)*force;
				dy=(dy/hyp)*force;
				p[P.VEL_X.v]-=dx;
				p[P.VEL_Y.v]-=dy;
			}
		}
	}
	public void addVelToPos(double[] p, double simtime) {
		simtime/=20;
		p[P.POS_X.v]+=(p[P.VEL_X.v]*simtime);
		p[P.POS_Y.v]+=(p[P.VEL_Y.v]*simtime);
	}
	public void update(double secondDiff) {
		for(double[] p : particles) {
			addDesiredForceVel(p);
			//addRepulsion(p);
			addFriction(p);
			addVelToPos(p,secondDiff);
		}
	}
}
